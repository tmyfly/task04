package com.tm.newscounter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class NewsCounterApplication {

	public static void main(String[] args) {
		final ApplicationContext context = SpringApplication.run(NewsCounterApplication.class, args);
		final NewsCounterUcs newsCounterUcs = context.getBean(NewsCounterUcs.class);
		newsCounterUcs.run();
	}
}
