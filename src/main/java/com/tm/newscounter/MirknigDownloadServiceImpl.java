package com.tm.newscounter;

import org.springframework.context.annotation.Profile;

@Profile("mirknig")
public class MirknigDownloadServiceImpl extends AbstractDownloadService{
    public MirknigDownloadServiceImpl(final String url, final ReturnContent returnContent) {
        super(url, returnContent);
    }
}
