package com.tm.newscounter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {

    @Autowired
    private WebPageDownloadService webPageDownloadService;
    @Autowired
    private NewsCounterService newsCounterService;
    @Autowired
    private NewsPrintingService newsPrintingService;


    @Bean
    NewsCounterUcs newsCounterUcs(){
        return new NewsCounterUcsImpl(webPageDownloadService, newsCounterService, newsPrintingService);

    }

}
