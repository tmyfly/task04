package com.tm.newscounter;

public interface WebPageDownloadService {
    public String download();
}
