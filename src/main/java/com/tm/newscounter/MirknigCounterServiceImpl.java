package com.tm.newscounter;

import org.springframework.context.annotation.Profile;

@Profile("mirknig")
public class MirknigCounterServiceImpl extends AbstractCounterService{

    public MirknigCounterServiceImpl(final String dataPars) {
        super(dataPars);
    }
}
