package com.tm.newscounter;


import org.springframework.beans.factory.annotation.Autowired;

public class NewsCounterUcsImpl implements NewsCounterUcs {

    private final WebPageDownloadService webPageDownloadService;
    private final NewsCounterService newsCounterService;
    private final NewsPrintingService newsPrintingService;

    public NewsCounterUcsImpl(final WebPageDownloadService webPageDownloadService,
                              final NewsCounterService newsCounterService,
                              final NewsPrintingService newsPrintingService) {
        this.webPageDownloadService = webPageDownloadService;
        this.newsCounterService = newsCounterService;
        this.newsPrintingService = newsPrintingService;
    }

    public void run(){
        final String page = webPageDownloadService.download();
        final int result = newsCounterService.count(page);
        newsPrintingService.printNewsCount(result);
    }
}
