package com.tm.newscounter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("mirknig")
public class MirknigiAppConfiguration {

    @Value("${url}")
    private String url;


    @Value("${searchPattern}")
    private String searchPattern;

    @Bean
    ReturnContent returnContent(){
        return new ReturnContentImpl();
    }

    @Bean
    WebPageDownloadService webPageDownloadService() {
       return new MirknigDownloadServiceImpl(url, returnContent());
    }

    @Bean
    NewsCounterService newsCounterService(){
        return new MirknigCounterServiceImpl(searchPattern);
    }

    @Bean
    NewsPrintingService newsPrintingService(){
        return new NewsPrintingService(url);
    }
}
