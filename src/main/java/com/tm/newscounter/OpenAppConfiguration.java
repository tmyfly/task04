package com.tm.newscounter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("opennet")
public class OpenAppConfiguration {

    @Value("${url}")
    private String url;

    @Value("${searchPattern}")
    private String searchPattern;

    @Bean
    ReturnContent returnContent(){
        return new ReturnContentImpl();
    }

    @Bean
    WebPageDownloadService webPageDownloadService() {
       return new OpenDownloadServiceImpl(url, returnContent());
    }

    @Bean
    NewsCounterService newsCounterService(){
        return new OpennetCounterServiceImpl(searchPattern);
    }

    @Bean
    NewsPrintingService newsPrintingService(){
        return new NewsPrintingService(url);
    }

}
