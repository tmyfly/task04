package com.tm.newscounter;

public interface NewsCounterService {
    public int count(final String str);
}
